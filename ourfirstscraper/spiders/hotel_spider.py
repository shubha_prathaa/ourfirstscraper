import scrapy
import json
from ourfirstscraper.items import OurfirstscraperItem

class HotelSpider(scrapy.Spider):
    name = "hotels"

    def start_requests(self):
        hotelIds = [9266562,4326873,6512109]
        url = 'https://www.almosafer.com/api/hotel/ahs/availability'
        for hotelId in hotelIds:
            header = {
                'accept': 'application/json, text/javascript',
                'content-type': 'application/json;charset=UTF-8',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
                'referer': 'https://www.almosafer.com/en/hotel/details/{hotelId}?uuid=9b169543-e0db-469e-9bda-d016bfbb0cd1&checkin=19-11-2018&checkout=20-11-2018&rooms=2_adult'
            }
            body = json.dumps({
                'dates':{
                    'checkin':'19-11-2018',
                    'checkout':'20-11-2018'
                },
                'room':[{
                    'guest':[
                        {
                        'type':'ADT'
                        },
                        {
                        'type':'ADT'
                        }
                    ]
                }],
                'hotelId':hotelId,
                'searchUuid':'9b169543-e0db-469e-9bda-d016bfbb0cd1'  
            })
            yield scrapy.Request(url=url,method='POST', headers=header,body=body, callback=self.parse)

    def parse(self, response):

        item = OurfirstscraperItem()
        res = json.loads(response.body)
        item['avId'] = res["avId"]
        item['request'] = res["request"]
        item['hotel'] = res["hotel"]

        yield item